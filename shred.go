package main

import (
	"crypto/rand"
	"fmt"
	"os"
)

func Shred(path string, deleteFile bool) error {
	file, err := os.OpenFile(path, os.O_WRONLY, 0666)
	if err != nil {
		return err
	}
	defer file.Close()

	// Get the file size
	fileInfo, err := file.Stat()
	if err != nil {
		return err
	}
	fileSize := fileInfo.Size()

	// Overwrite the file three times with random data
	for i := 0; i < 3; i++ {
		randomData := make([]byte, fileSize)
		_, err := rand.Read(randomData)
		if err != nil {
			return err
		}

		_, err = file.WriteAt(randomData, 0)
		if err != nil {
			return err
		}

		err = file.Sync()
		if err != nil {
			return err
		}
	}

    if deleteFile {
        // Remove the file
        err = os.Remove(path)
        if err != nil {
            return err
        }

        fmt.Printf("File %s shredded and deleted.\n", path)
    } else {
        fmt.Printf("File %s shredded but not deleted.\n", path)
    }

	return nil
}

func main() {
    err := Shred("randomfile", true)
    if err != nil {
        fmt.Println(err)
        return
    }
}

