package main

import (
	"bytes"
	"crypto/rand"
	"io/ioutil"
	"os"
	"testing"
)

// Shred must return an error when attempting to shred a file that
// does not exist.
func TestShredNonexistentFile(t *testing.T) {
	err := Shred("nonexistentfile", true)
	if err == nil {
		t.Errorf("Expected an error, but Shred succeeded")
	}
}

// Shred must return an error when attempting to shred a file with
// restricted file permissions.
func TestShredRestrictedFile(t *testing.T) {
	file, err := ioutil.TempFile("", "testfile")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())

	// Set restrictive file permissions
	err = os.Chmod(file.Name(), 0000)
	if err != nil {
		t.Fatal(err)
	}

	err = Shred(file.Name(),true)
	if err == nil {
		t.Errorf("Expected an error, but Shred succeeded")
	}
}

// Shred must delete the file after shred it successfully.
func TestShredFile(t *testing.T) {
	file, err := ioutil.TempFile("", "testfile")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())

	originalData := make([]byte, 1024)
	_, err = rand.Read(originalData)
	if err != nil {
		t.Fatal(err)
	}

	_ ,err = file.Write(originalData)
	if err != nil {
		t.Fatal(err)
	}

	err = Shred(file.Name(), true)
	if err != nil {
		t.Fatal(err)
	}
	_, err = os.Stat(file.Name())
	if !os.IsNotExist(err) {
		t.Errorf("Expected file to be deleted, but it still exists")
	}
}

// Shred must work for empty files.
func TestShredEmptyFile(t *testing.T) {
	file, err := ioutil.TempFile("", "testfile")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())

	err = Shred(file.Name(), true)
	if err != nil {
		t.Fatal(err)
	}

	_, err = os.Stat(file.Name())
	if !os.IsNotExist(err) {
		t.Errorf("Expected file to be deleted, but it still exists")
	}
}

// Shred must work for large files.
func TestShredLargeFile(t *testing.T) {
    file, err := ioutil.TempFile("", "testfile")
    if err != nil {
        t.Fatal(err)
    }
    defer os.Remove(file.Name())

    // Write a large amount of data to the file
    originalData := make([]byte, 1<<20) // 1 MB
    _, err = rand.Read(originalData)
    if err != nil {
        t.Fatal(err)
    }

    _, err = file.Write(originalData)
    if err != nil {
        t.Fatal(err)
    }

    err = Shred(file.Name(), true)
    if err != nil {
        t.Fatal(err)
    }

    _, err = os.Stat(file.Name())
    if !os.IsNotExist(err) {
        t.Errorf("Expected file to be deleted, but it still exists")
    }
}

// Verify that the content of the input file is overwritten.
func TestShredFileOverwrite(t *testing.T) {
	file, err := ioutil.TempFile("", "testfile")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())

	content := []byte("test content")
    _, err = file.Write(content)
    if err != nil {
        t.Fatal(err)
    }

	// Shred the file without deleting it
	err = Shred(file.Name(), false)
	if err != nil {
		t.Fatal(err)
	}

	// Verify that the file content has been overwritten
	fileContent, err := ioutil.ReadFile(file.Name())
	if err != nil {
		t.Fatal(err)
	}

	if bytes.Equal(content, fileContent) {
		t.Error("Expected file content to be overwritten, but it's the same as before")
	}
}
